import { Inject, Injectable } from '@nestjs/common';
import { Collection, ObjectId } from 'mongodb';
import { Blog } from './blog.model';

@Injectable()
export class BlogService {
    constructor(@Inject('DATABASE_CONNECTION')
    private coll: Collection) { }

    // getting all the data
    async allBlogs(): Promise<any> {
        const data = await this.coll.find({});
        return data.toArray();
    }

    // getting data of based on id
    async getBlogById(id: string): Promise<any> {
        try {
            const data = await this.coll.findOne({ _id: new ObjectId(id) });
            if (data == null) return "Blog not found";
            return data;
        } catch (error) {
            return {
                message: "Error in finding user data",
                error: error
            };
        }
    }

    //createing new blog
    async createBlog(newBlogData): Promise<string> {
        try {
            // here is some errors 
            const newBlog: Blog = newBlogData;

            /// for publication data
            newBlog.publicationDate = new Date();

            await this.coll.insertOne(newBlog);
            return 'Data saved successfully';
        } catch (error) {
            console.error('Error creating blog:', error);
            throw new Error('Error creating blog');
        }
    }


    // updating the blog
    async updateBlog(id: string, body: {}): Promise<any> {
        try {
            const data = await this.coll.findOne({ _id: new ObjectId(id) });
            for (const key in body) {
                if (body.hasOwnProperty(key)) {
                    data[key] = body[key];
                }
            }
            data.updateddate = new Date();
           await this.coll.updateOne({ _id: new ObjectId(id) }, { $set: data })
            return "updation done";
        } catch (error) {
            return error;
        }
    }

    // deleting the blog
    async deleteBlog(id: string): Promise<string> {
        try {
            await this.coll.deleteOne({ _id: new ObjectId(id) });
            return "the referenced blog deleted";
        } catch (error) {
            console.error('Error in deleting this blog:', error);
            throw new Error('Error in deleting this blog');
        }
    }

    // uploding the blog
    async uploadimage(id: string, imageurl: string): Promise<string> {
        try {
            await this.coll.updateOne({ _id: new ObjectId(id) }, { $set: { imageUrl: imageurl } })
            return "image uploded successfully";
        } catch (error) {
            console.error('Error in uploding in the image:', error);
            throw new Error('Error in uploding in the image');
        }
    }


    // Adding tags based on id
    async addtag(id: string, body: any): Promise<string> {
        try {
            const data = await this.coll.findOne({ _id: new ObjectId(id) });
            console.log(data)
            if (data.tag.includes(body.tag))
                return "tag already exist in tags";
            data.tag.push(body.tag);
            await this.coll.updateOne({ _id: new ObjectId(id) }, { $set: data });
            return "New Tag has been Added";
        } catch (error) {
            console.error('Error in adding the tag:', error);
            throw new Error('Error in adding the tag');
        }
    }

    // removing the tags
    async deletetag(id: string, body: any): Promise<string> {
        try {
            const data = await this.coll.findOne({ _id: new ObjectId(id) });
            if (!data.tag.includes(body.tag))
                return "tag does not exist in tags";

            data.tag = data.tag.filter((key) => { return key !== body.tag })
            await this.coll.updateOne({ _id: new ObjectId(id) }, { $set: data })
            return "the referenced tag deleted";
        } catch (error) {
            console.error('Error in deleting this tag:', error);
            throw new Error('Error in deleting this tag');
        }
    }


}
