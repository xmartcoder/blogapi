import { Module } from '@nestjs/common';
import { MongoClient, Collection } from 'mongodb';

@Module({
    providers: [
        {
            provide: 'DATABASE_CONNECTION',
            useFactory: async (): Promise<Collection> => {
                try {
                    const client = await MongoClient.connect('mongodb://127.0.0.1');
                    const db = await client.db('BlogApi');
                    return await db.collection('blogs');
                } catch (e) {
                    throw e;
                }
            }
        },
    ],
    exports: ['DATABASE_CONNECTION'],
})
export class DatabaseModule { }