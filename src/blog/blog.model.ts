export type Blog = {
    postID: String,
    title: String,
    content: String,
    imageUrl: String,
    authorID: String,
    authorName: String,
    categoryID: String,
    createdBy: String,
    publicationDate: Date,
    updateddata: Date,
    comments: String,
    tag: []
  };

