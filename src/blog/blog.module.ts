import { Module } from '@nestjs/common';
import { BlogController } from './blog.controller';
import { BlogService } from './blog.service';
import { MulterModule } from '@nestjs/platform-express';
import { DatabaseModule } from './mongodb';


@Module({
  imports:
    [
      DatabaseModule,
      MulterModule.register({ dest: './public/uploads' })
    ],
  controllers: [BlogController],
  providers: [BlogService],
})
export class BlogModule { }