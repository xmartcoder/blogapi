export interface User {
    "postID": "string",
    "title": "string",
    "content": "string",
    "authorID": "string",
    "categoryID": "string",
    "publicationDate": "date"
}
