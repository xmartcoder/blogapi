import { Controller, Get, Post, Body, Delete, Param, Patch, UseInterceptors, UploadedFile } from '@nestjs/common';
import { BlogService } from './blog.service';
import { Blog } from './blog.model';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import * as path from 'path';
import { randomAlphaNumeric } from './randomstring';


@Controller('blog')
export class BlogController {
    constructor(private readonly blogService: BlogService) { }

    // getting all the blogs
    @Get()
    allBlogs(): Promise<Blog[]> {
        return this.blogService.allBlogs();
    }

    // Retrieving a specific blog based on id
    @Get(':id')
    getBlogById(@Param('id') id: string): Promise<Blog> {
        return this.blogService.getBlogById(id);
    }

    // Creating a new blog  
    @Post()
    createBlog(@Body() blogData: Blog): Promise<string> {
        return this.blogService.createBlog(blogData);
    }

    // Updating the stored blog
    @Patch(':id')
    updateBlog(@Param('id') id: string, @Body() body: {}): Promise<string> {
        return this.blogService.updateBlog(id, body);
    }

    // Deleting the blog based on id
    @Delete(':id')
    deleteBlog(@Param('id') id: string): Promise<string> {
        return this.blogService.deleteBlog(id);
    }

    // adding the new tag in taglist
    @Patch('/tag/:id')
    addtag(@Param('id') id: string, @Body() body: {}): Promise<string> {
        return this.blogService.addtag(id, body);
    }

    // Deleting the tag based on id
    @Delete('/tag/:id')
    deletetag(@Param('id') id: string, @Body() data: {}): Promise<string> {
        return this.blogService.deletetag(id, data);
    }

    // uploding image into blog
    @Post('/image/:id')
    @UseInterceptors(
        FileInterceptor('blogimage',
            {
                storage: diskStorage({
                    destination: './public/uploads',
                    filename: (req, file, callback) => {
                        const modifiedFilename = randomAlphaNumeric(20) + path.extname(file.originalname);
                        callback(null, modifiedFilename);
                    },
                }),
            }),
    )
    uploadimage(@Param('id') id: string, @UploadedFile() image: Express.Multer.File): Promise<string> {
        const imageurl = image.filename;
        return this.blogService.uploadimage(id, imageurl);
    }

}